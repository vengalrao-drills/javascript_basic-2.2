
// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.

// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.

// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.

// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.



const inventory = require('./inventory')
var CarName = []
var carYears = []
var olderCars = []
var BmwAudi = []
 
function compileAll(){
    for (let i = 0; i < inventory.length; i++) {
        CarName.push(inventory[i].car_model)
        carYears.push(inventory[i].car_year)
        if (inventory[i].car_year < 2000) {
            olderCars.push(inventory[i].car_year)
        }
        if (inventory[i].car_make.toLowerCase() === 'bmw' || inventory[i].car_make.toLowerCase() === 'audi') {
            BmwAudi.push(inventory[i].car_model)
        }
    }
    CarName.sort()
}

compileAll()

   
function allInOne(day) {
  
    switch (day) {
        case 'sortCarNames':
            return CarName
            break
        case 'sortByYear':
            return carYears
            break

        case 'old_Cars':
            return olderCars
            break
        case 'BMWAndAudi':
            return BmwAudi
            break
        default :
            return 'Error'
            break
    }
}

module.exports = allInOne ;


/// the main thing compared to problem3 here is redcuing the code, Time, code Readablity, understanding of the code.
// Implemented switch case
// the  compileAll will only run once ! it will reduce time complexity 
// faster the execution


