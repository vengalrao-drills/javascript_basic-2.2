
// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.
var CarName = []
const inventory = require('./inventory')

function sortCarNames(){
    for (let i=0 ; i<inventory.length ; i++){
        CarName.push(inventory[i].car_model)  // here we are adding car names i.e car_model adding into the array
    }
    CarName.sort()
     
    return CarName;
}
module.exports = sortCarNames;

// ==== Problem #4 ====
// The accounting team needs all the years from every car on the lot. Execute a function that will return an array from the dealer data containing only the car years and log the result in the console as it was returned.
var carYears = []
function sortByYear(){
    for (let i=0 ; i<inventory.length ; i++){
        carYears.push(inventory[i].car_year) // here adding only years
    }
     
    return carYears;
}
module.exports = sortByYear;


// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. Using the array you just obtained from the previous problem, find out how many cars were made before the year 2000 and return the array of older cars and log its length.
var olderCars = []
function old_Cars(){
    for (let  i=0; i<inventory.length ; i++){
        if (inventory[i].car_year <2000){
            olderCars.push(inventory[i].car_year) // needed cars older than 2000s.
        }        
    }
    return olderCars ;
}
module.exports = old_Cars ;

// ==== Problem #6 ====
// A buyer is interested in seeing only BMW and Audi cars within the inventory.  Execute a function and return an array that only contains BMW and Audi cars.  Once you have the BMWAndAudi array, use JSON.stringify() to show the results of the array in the console.


var BmwAudi  = []
function BMWAndAudi(){
    for (let  i=0; i<inventory.length ; i++){
        if (inventory[i].car_make.toLowerCase()  ==='bmw' ||  inventory[i].car_make.toLowerCase() ==='audi' ){
            BmwAudi.push(inventory[i].car_model) // push only car names that made in only BMW  and Audi company
        }        
    }
    return BmwAudi ;
}
module.exports = BMWAndAudi ;



module.exports = {
    sortCarNames , sortByYear , old_Cars , BMWAndAudi
}