// ==== Problem #2 ====
// The dealer needs the information on the last car in their inventory. Execute a function to find what the make and model of the last car in the inventory is?  Log the make and model into the console in the format of:
// "Last car is a *car make goes here* *car model goes here*"

// ans

const inventory =require('./inventory')    //   Imported inventory - inventory is data
const problem2 = require('./problem2')     //   Imported problem
//So basically require() help us to import functions from other javaScript file

console.log(problem2(inventory))